import { applyMiddleware, createStore, compose } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from 'redux-thunk'
import rootReducer from './reducers'

const initialState = {}
const middleware = [thunk]
// const store = createStore(rootReducer, initialState, compose(applyMiddleware(...middleware),
//  window.__REDUX_DEVTOOLS_EXTENTION__ && window.__REDUX_DEVTOOLS_EXTENTION__()
//  ))

const store = createStore(rootReducer, initialState, composeWithDevTools(applyMiddleware(...middleware)
 ))

export default store