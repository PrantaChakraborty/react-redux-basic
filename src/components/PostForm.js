import React, { Component } from "react"

class PostForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			title: "",
			body: "",
		}
		this.handleChange = this.handleChange.bind(this)
		this.onSubmit = this.onSubmit.bind(this)
	}

	handleChange(event) {
		event.preventDefault()
		this.setState({ [event.target.name]: event.target.value })
	}

	onSubmit(event) {
        // creting post
		event.preventDefault()
		const post = {
			title: this.state.title,
			body: this.state.body,
		}

		fetch("https://jsonplaceholder.typicode.com/posts", {
			method: "POST",
			headers: {
				"content-type": "application/json",
			},
			body: JSON.stringify(post),
		})
			.then((res) => res.json())
			.then((data) => console.log(data))
	}
	render() {
		return (
			<div>
				<h2>Add Post</h2>
				<form onSubmit={this.onSubmit}>
					<div>
						<label>Title</label>
						<br />
						<input
							type="text"
							name="title"
							value={this.state.title}
							onChange={this.handleChange}
						/>
					</div>
					<div>
						<label>Body</label>
						<br />
						<textarea
							name="body"
							value={this.state.body}
							onChange={this.handleChange}
						/>
					</div>
					<br />
					<button>Add Post</button>
				</form>
			</div>
		)
	}
}

export default PostForm
