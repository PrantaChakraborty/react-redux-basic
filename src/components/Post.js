import React, { Component } from "react"
import ProptTypes from "prop-types"
import { connect } from "react-redux"
import { fetchPosts } from "../actions/postActions"

class Post extends Component {
	componentDidMount() {
		this.props.fetchPosts()
	}

	render() {
		const postItems = this.props.posts.map((post) => (
			<div key={post.id}>
				<h3>{post.title}</h3>
				<p>{post.body}</p>
			</div>
		))
		return (
			<div>
				<h3>Posts</h3>
				{postItems}
			</div>
		)
	}
}

Post.ProptTypes = {
    fetchPosts: ProptTypes.func.isRequired,
    posts: ProptTypes.array.isRequired
}

const mapStateToProps = (state) => ({
	// here 'posts' the name will be same as named in combine reducer in index.js(RootReducer)
	posts: state.posts.items,
})

export default connect(mapStateToProps, { fetchPosts })(Post)
