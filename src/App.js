import "./App.css"
import Post from "./components/Post"
import PostForm from "./components/PostForm"
import { Provider } from "react-redux"

import React, { Component } from "react"

import store from './store'

class App extends Component {
	render() {
		return (
			<div>
				<Provider store={store}>
					<div className="App">
						<PostForm />
						<hr />
						<Post />
					</div>
				</Provider>
			</div>
		)
	}
}

export default App
